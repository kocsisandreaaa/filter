$(".comment-button").click(function(){
	var data_id = $(this).data("id");
	
	$('.active-or-hide-'+data_id).show();
	$('.new_comment-'+data_id).show();
	
	//user képe kör alaku
	var cw = $('.user-img-'+data_id).width();
	$('.user-img-'+data_id).css('height', cw + 'px');
});

$(".comments-hide-button").click(function(){
	var data_id = $(this).data("id");
	$('.active-or-hide-'+data_id).hide();
	$('.new_comment-'+data_id).hide();
});

$(".new-comment-button").click(function(){
	var data_id = $(this).data("id");
	$('.new_comment-'+data_id).show();
	$('.active-or-hide-'+data_id).hide();
});
