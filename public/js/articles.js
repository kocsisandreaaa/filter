var count = 0
function getArticle(){
	$.ajax({
		type: 'GET',
		url: window.location.origin + '/xhr'+window.location.pathname,
		data: {
			count: count
		},
		success: function(data){
			$('#result').append(data)
			scrollTrigger(count)
			
			count = count + 1
		}
	});
}
function scrollTrigger(count)
{
	var stop = 0
	setTimeout(function(){
		var waypoint = new Waypoint({
			element: document.getElementById('article'+count),
			handler: function(direction) {
				if (stop === 0) {
					getArticle();
					stop = 1
				}
			}
		})
	},0);
}

getArticle();

