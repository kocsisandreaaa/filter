// console.log('fl')
function save(){
	showLoading();

	$.ajax({
		type: 'POST',
		data: $('#filter_form').serialize(),
		success: function(data){
			hideLoading();
			// location.reload();
		}
	});
}
$('#save').click(function(e) {
	save();
});

$('#keyword_box_btn').click(function(e) {
	$('#keyword_box').toggle('fast');
});

$(document).on('change', '#topic', function(e) {
	var id = $(this).val();

	$('#new_topic').val('');
	if (id == 'other') {
		$('#new_topic').show();
	}else{
		$('#new_topic').hide();
	}
});

function showLoading()
{
	$("#overlay").show();
	$("#loader").show();
}

function hideLoading()
{
	$("#overlay").hide();
	$("#loader").hide();
}


// load plus one box
$( document ).ready(function() {
	$.ajax({
		type: 'GET',
		url: '/xhr/subviews/keyword_panel',
		success: function(data){
			$('#keyword_box_body').append(data)
		}
	});
});

/*hírportál popup megnyitása*/
function news_portal_show(){
	$('#portal_select').click(function(e) {
		$.ajax({
			url: "/news_portal_show",
			type: 'GET',
			data: '',
			success: function(data){
				bootbox.confirm({
					message: data,
				    buttons: {
				        confirm: {
				            label: 'Mentés',
				            className: 'btn-success'
				        },
				        cancel: {
				            label: 'Mégse',
				            className: 'btn-danger'
				        }
				    },
				    callback: function (result) {
				    	if (result == true) {
				        	new_portal_save($('#portal_checkbox_form').serialize());
				    	}
				    }
				});
			}
		});
	});
}
news_portal_show()

/*hírportál popup mentése*/
function new_portal_save($requests) {
	$.ajax({
		url: "/news_portal_save",
		type: 'POST',
		data: $requests,
		success: function(data){
			console.log('A Controllerben még nincs megírva a mentés! :)')
			console.log(data)
		}
	});
}

/*az egyéb beállított szürők mentése*/
function new_filter_save(){
	// showLoading();
	$('#filter_save').click(function(e) {
		$.ajax({
			type: 'POST',
			data: $('#new_filter_form').serialize(),
			success: function(data){
				// hideLoading();
			}
		});
	});
}
new_filter_save()