<?php

namespace App;

use App\Website;
use App\Visit;
use App\Scann;
use Illuminate\Notifications\Notifiable;
use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquent;

class Page extends NeoEloquent
{
    use Notifiable;


    protected $label = 'Page';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url', 'title', 'pub_date', 'description',
    ];

    public function visit()
    {
        return $this->hasMany(Visit::class,'PAGE_VISIT');
    }

    public function scann()
    {
        return $this->hasMany(Scann::class,'PAGE_SCANNED');
    }

    public function website()
    {
        return $this->belongsTo(Website::class, 'PAGE_OF_WEBSITE');
    }
}
