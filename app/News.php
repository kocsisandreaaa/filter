<?php

namespace App;

use App\Page;
use App\Website;
use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquent;

class News extends NeoEloquent
{
    protected $label = 'News';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key',
    ];

    public function website()
    {
        return $this->hasMany(Website::class, 'NEWS_SITE');
    }
}
