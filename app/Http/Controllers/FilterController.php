<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FilterController extends Controller
{
    private $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware(function ($request, $next) {
            $this->user = User::with('website')->where('email', Auth::user()->email)->first();

            return $next($request);
        });
    }

    public function filter()
    {
        $topics = Topic::where('status','<>','deleted')->get()->toArray();
        // $website = Website::with('topic')->with('word')->where('domain',$website)->first();

        // $website_topic = '';
        // if ($website->topic->count() > 0) {
        //     $website_topic = $website->topic[0]->key;
        // }
        // $keywords = [];
        // if ($website->word->count() > 0) {
        //     $keywords = $website->word;
        // }

        return view('filter.filter',[
            'topics' => $topics,
            // 'website_topic' => $website_topic,
            // 'keywords' => $keywords,
        ]);
    }

    public function saveFilter(Request $request)
    {
        $website = Website::with('topic')->with('word')->where('domain',$website)->first();
        if (!empty($website)) {
            $this->editTopic( $request->topic ?? 0, $request->new_topic ?? false, $website);
            $this->addFilterWords($this->getKeywordFromRequest($request->all()), $website);
        }
    }

    protected function editTopic($topic_id, $new_topic)
    {
        if ($topic_id == 'other' && $new_topic) {
            $topic = new Topic(['status' => 'pending', 'key' => $new_topic]);
        }else{
            $topic = Topic::find($topic_id);
        }

        if (!empty($topic)) {
            foreach ($website->topic()->edges() as $key => $relation) {
                $relation->delete();
            }
            $website->topic()->save($topic);
        }
    }

    protected function getKeywordFromRequest($words)
    {
        $keywords = [];
        foreach ($words as $key => $word) {
            $kw_number = substr($key,8); // is number of the keyword
            $rw_number = substr($key,7); // is number of the related words
            $k_number = substr($key,5,1);
            $w_key = substr($key,0,7);

            if ($w_key == 'keyword' && is_numeric($kw_number)) {
                if (!empty($word)) {
                    $keywords[$kw_number]['keyword'] = $word;
                }
            }
            if (is_numeric($k_number) && $w_key == 'word_'.$k_number.'_'  && is_numeric($rw_number)) {
                if (!empty($word)) {
                    $keywords[$k_number]['related_words'][$rw_number]['text'] = $word;
                }
            }
        }
        return $keywords;
    }

    protected function addFilterWords()
    {
        if (!empty($keywords)) {
            $website->deleteAllWordRelation();
            foreach ($keywords as $k_word) {
                if (!empty($k_word)) {
                    $word_obj = Word::with('website')->where('text',$k_word['keyword'])->first();

                    if (empty($word_obj)) {
                        $word_obj = new Word(['text' => $k_word['keyword']]);
                    }
                    $website->word()->save($word_obj);
                    
                    if (!empty($k_word['related_words'])) {
                        $this->addRelatedWords($k_word['related_words'], $word_obj);
                    }
                }
            }
        }
        $word = new Word;
        $word->deleteNotRelatedWords();
    }

    protected function addRelatedWords($words, $word_obj)
    {
        if (!empty($words)) {
            $word_obj->deleteAllWordRelation();
            foreach ($words as $key => $word) {
                $related_word_obj = Word::where('text',$word['text'])->first();

                if (empty($related_word_obj)) {
                    $related_word_obj = new Word(['text' => $word['text']]);
                }
                $word_obj->relatedWord()->save($related_word_obj);
            }
        }
    }

    public function articles()
    {
        return view('filter.articles');
    }

    public function articleXHR(Request $request)
    {
        $limit = 10;
        $pages = Page::offset($request->count * $limit)
            ->limit($limit)
            ->get()
        ;
        return view('filter.subviews.article_box',[
            'articles' => $pages,
            'count' => $request->count,
        ]);
    }

    public function newsPortalShow()
    {
        return view('filter.subviews._news_portal_select_subview');
    }
}
