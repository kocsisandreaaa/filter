<?php

namespace App;

use DB;
use App\Scann;
use App\Website;
use App\Topic;
use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquent;

class Word extends NeoEloquent
{
    protected $label = 'Word';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text',
    ];

    public function scann()
    {
        return $this->belongsTo(Scann::class,'WORD_OF_SCANNED_PAGE');
    }

    public function topic()
    {
        return $this->belongsTo(Topic::class,'WORD_OF_TOPIC');
    }

    public function website()
    {
        return $this->belongsTo(Website::class,'KEYWORD_OF_USER_WEBSITE');
    }

    public function relatedWord()
    {
        return $this->belongsToMany(Word::class,'WEBSITE_KEYWORD_RELATED_WORD');
    }

    public function deleteAllWordRelation()
    {
        foreach ($this->relatedWord()->edges() as $key => $relation) {
            $relation->delete();
        }
    }

    public function deleteNotRelatedWords()
    {
        return DB::select('
            MATCH (n:Word)
                WHERE size((n)--())=0
            DELETE (n)
        ');
    }
}
