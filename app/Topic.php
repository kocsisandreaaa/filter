<?php

namespace App;

use App\Word;
use App\Website;
use Vinelab\NeoEloquent\Eloquent\Model as NeoEloquent;

class Topic extends NeoEloquent
{
    protected $label = 'Topic';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'key', 'status', 'hu','en',
    ];

    public function word()
    {
        return $this->hasMany(Word::class, 'KEYWORD_OF_TOPIC');
    }

    public function website()
    {
        return $this->belongsTo(Website::class, 'TOPIC_OF_WEBSITE');
    }
}
