<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::group(['middleware' => 'auth'], function () {
	Auth::routes();
	Route::get('/logout', 'Auth\LoginController@logout');
	Route::get('/', function () {
	    return redirect('/login');
	});
	Route::get('/filter', 'FilterController@filter');
	Route::get('/articles', 'FilterController@articles');
	Route::get('/news_portal_show', 'FilterController@newsPortalShow');

	Route::get('/xhr/articles', 'FilterController@articleXHR');
	// Route::post('/filter', 'PrivateFilterController@filterSave');

	//xhr
	Route::get('/xhr/subviews/keyword_panel', function () { return view('subviews.keyword_box');});

	Route::post('/filter', 'FilterController@saveFilter');
	Route::post('/news_portal_save', function () { return 'ok';});
// });