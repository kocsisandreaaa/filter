@extends('private_app')

@section('css')
	<link href="{{ asset('/css/articles.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="page-title row"><h3><b>A legfrissebb cikkek</b></h3></div>
<div id="result"></div>

@endsection

@section('js')
	<script src="{{ asset('js/noframework.waypoints.min.js') }}"></script>
	<script src="{{ asset('js/articles.js') }}" type="text/javascript"></script>
@endsection