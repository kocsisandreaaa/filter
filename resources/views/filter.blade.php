@extends('private_app')

@section('css')
	<link href="{{ asset('/css/filter.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="page-title row"><h3><b>Szűrő feltételek beállítása</b></h3></div>
<form id="new_filter_form">
	{{ csrf_field() }}
    <div class="form-group">
     	<label for="sel1">Témák:</label>
      	<select id="topic" class="form-control" name="topic">
			<option value="select">{{ ucfirst(__('site.select.select.topic')) }}</option>
				@foreach($topics as $topic)
					{{-- @if($topic['key'] == $website_topic) --}}
					<option value="{{$topic['id']}}" selected>{{ ucfirst($topic['hu'] ?? __('site.error.not_translated'))   }}</option>
					{{-- @else
					<option value="{{$topic['id']}}">{{ ucfirst($topic['hu'] ?? __('site.error.not_translated')) }}</option>
					@endif --}}
				@endforeach
			<option value="other">{{ ucfirst(__('site.select.other')) }}</option>
		</select>
		<input id="new_topic" style="display: none;" class="form-control" name="new_topic">
      	<br>
      	<label for="usr">Kulcsszó:</label>
  		<div id="keyword_panel_group" class="form-group">
			<button type="button" class="btn btn-primary btn-sm" id="keyword_box_btn">Kulcsszavak hozzáadása</button>
			<div id="keyword_box" class="panel panel-primary" style="display: none">
				<div id="keyword_box_body" class="panel-body">
					{{-- @if(!empty($keywords))
						@foreach($keywords as $keyword) --}}
							@include('subviews.keyword_box')
						{{-- @endforeach
					@endif --}}
				</div>
			</div>
		</div>
		<div class="form-group">
			<input id="save" type="button" class="btn btn-success pull-right" value="{{ ucfirst(__('site.button.save')) }}">
		</div>
  		<br>
      	<label for="usr">News portal:</label>
      	<div class="btn btn-default" id="portal_select">Kiválaszt</div>
      	<br>
      	<label for="sel1">Időintervallum:</label>
      	<select class="form-control" id="sel1" name="date_intervallum">
        	<option name="">-- Válasszon időintervallumot! --</option>
			<option name="day">Ma megjelent cikkek</option>
			<option name="week">Az utolsó hét nap cikkei</option>
			<option name="month">Az elmult 30 nap cikkei</option>
			<option name="year">Az elmult 365 nap cikkei</option>
      	</select>
      	<br>
    </div>
</form>
<div class="text-right"><button id="filter_save" class="btn btn-default">Mentés</button></div>
@endsection

@section('js')
	<script src="{{ asset('/js/filter.js') }}" type="text/javascript"></script>
@endsection