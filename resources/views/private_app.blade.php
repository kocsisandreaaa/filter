<!DOCTYPE html>
<html>
    <head>
    	<title></title>
    	<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap 3.3.2 -->
        <link href="{{ asset("/adminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
        
        <!-- Styles -->
        <link href="{{ asset('/css/master.css') }}" rel="stylesheet">
        @yield('css')
    </head>
    <body>

        <nav id="header" class="navbar navbar-fixed-top">
            <div id="header-container" class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a id="brand" class="navbar-brand" href="/articles">
                        LOGO
                    </a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="/articles">Hírek</a></li>
                        <li><a href="/filter">Szűrés</a></li>
                        <li><a href="#">Mentett oldalak</a></li>
                        <li class="dropdown">
                            <a href="javascript:void(0)" class="dropbtn"><b>Kis Pista</b></a>
                            <div class="dropdown-content">
                                <a href="#">Profil</a>
                                <a href="#">Beállítások</a>
                                <a href="#">Kilépés</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-content">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="/js/master.js"></script>

    @yield('js')
    </body>
</html>