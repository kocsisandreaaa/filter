@if(isset($articles) && !empty($articles))
	@foreach($articles as $key => $article)
	<div id="article{{ $key == 5 ? $count : ''}}" class="row cikk-panel">
		<div class="col-md-4 col-xs-12">
	  		<a href="{{ $article->url }}"><img class="article-image-2 article-image" src="/photo/005.jpg"></a>
	  	</div>
	  	<div class="col-md-8 col-xs-12">
			<div class="cikk-header">
  				<b>
		      		<div class="text-right">{{ $article->pub_date }}</div>
		      		<a href="{{ $article->url }}"><div class="title">{{ $article->title }}</div></a>
		      	</b>
	  		</div>
	      	<div class="article-short text-justify">{{ $article->description }}</div>
	    </div>
  		<div class="col-md-12">
  			<div class="green-border">
  				<div class="buttons">
  					<i class="far fa-thumbs-up fa-2x"></i><span class="icon-name">Like(15)</span>
	      			{{-- <i class="fas fa-thumbs-up fa-2x"></i> --}}
	      			<span class="comment-button" data-id="2"><i class="fas fa-comments fa-2x"></i><span class="icon-name">Hozzászólások(2)</span></span>
	      			{{-- <i class="far fa-comments fa-2x"></i> --}}
	      			{{-- <i class="fas fa-comment fa-2x"></i> --}}
	      			<span class="new-comment-button" data-id="2"><i class="far fa-comment fa-2x"></i><span class="icon-name">Új hozzászólás</span></span>
	      			<span class="" data-id="2"><i class="fab fa-algolia fa-2x"></i><span class="icon-name">Elolvasom később</span></span>
  				</div>
		  		
		  		<div class="active-or-hide-2 active-or-hide">
		  			<div class="">
		  				<b>Hozzászólások</b>
		  				<span class="comments-hide-button comments-hide-button-padding-left" data-id="2"><i class="far fa-comment-alt"></i><span class="icon-name">Hozzászólások elrejtése</span></span>
		  			</div>
					<div class="border border-success row">
	  					<div class="comment-border">
	  						<div class="row">
	  							<div class="col-md-1  col-xs-12">
	      							<img class="user-img user-img-2" src="/photo/background1.jpg">
	      						</div>
	      						<div class="col-md-11  col-xs-12">
	      							<div><b>Lakatos Jules</b></div>
	      							<div>Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy!</div>
	      							<div class="ikon-in-comments">
	      								<span class="" data-id="2"><i class="fas fa-comments fa-2x"></i><span class="icon-name">Hozzászólások(2)</span></span>
	      								<span class="" data-id="2"><i class="far fa-comment fa-2x"></i><span class="icon-name">Új hozzászólás</span></span>
	      							</div>
	      						</div>
	  						</div>
	  					</div>
	  				</div>
					@for($si=1; $si<=3; $si++)
					<div class="row">
						<div class="col-md-offset-1 col-md-11  col-xs-12">
		  					<div class="comment-border">
		  						<div class="row">
		  							<div class="col-md-1  col-xs-12">
		      							<img class="user-img user-img-2" src="/photo/background1.jpg">
		      						</div>
		      						<div class="col-md-11  col-xs-12">
		      							<div><b>Lakatos Jules</b></div>
		      							<div>Examples might be simplified to improve reading and basic understanding. Tutorials, references, and examples are constantly reviewed to avoid errors, but we cannot warrant full correctness of all content. While using this site, you agree to have read and accepted our terms of use, cookie and privacy policy!</div>
		      						</div>
		  						</div>
		  					</div>
						</div>
					</div>
					@endfor
					<div class="col-md-12 top-and-bottom-padding">
						<span class="comments-hide-button col-md-12" data-id="2"><i class="far fa-comment-alt"></i><span class="icon-name">Hozzászólások elrejtése</span></span>
					</div>
		  		</div>
		  		<div class="new_comment_show_or_hide new_comment-2">
		  			<div class="new_comment">
		  				<div class=""><b>Új hozzászólás</b></div>
		  				<div class=""><textarea class="form-control"></textarea></div>
		  				<div class="text-right send-button"><input class="btn btn-default" type="submit" name="" value="Küldés"></div>
		  			</div>
		  		</div>
  			</div>
  		</div>
	</div>
@endforeach
@endif
<script src="{{ asset('js/article_box.js') }}" type="text/javascript"></script>