<form id="portal_checkbox_form">
	{{ csrf_field() }}
	<div class="page-title"><h3><b>Hírportálok kiválasztása</b></h3></div>
	<label class="checkbox-inline">
  		<input type="checkbox" name="origo">Origo
	</label>
	<label class="checkbox-inline">
  		<input type="checkbox" name="teol">TEOL
	</label>
	<label class="checkbox-inline">
  		<input type="checkbox" name="hvg">hvg
	</label>
	<label class="checkbox-inline">
  		<input type="checkbox" name="blikk">Blikk
	</label>
	<label class="checkbox-inline">
  		<input type="checkbox" name="index">Index
	</label>
</form>