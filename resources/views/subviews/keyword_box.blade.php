{{-- @if(empty($loop) || $loop->iteration < 10 ) --}}
	<p>
		<input type="text" name="keyword_{{ ( isset($loop) ? $loop->iteration : 0) }}" class="form-control" value="{{-- {{ ( isset($loop) ? ($keyword->text ?? '') : '') }} --}}">
	</p>
	<p>
		<button type="button" class="btn btn-info btn-xs" data-toggle="collapse" data-target="#keyword_{{ ( isset($loop) ? $loop->iteration : 0) }}">Pontosítás</button>
		<div id="keyword_{{ ( isset($loop) ? $loop->iteration : 0) }}" class="collapse panel panel-primary">
			<div class="panel-body">
				<span>Kapcsolodó szavak</span>
				{{-- @if(!empty($keyword->relatedWord)) --}}
					{{-- @foreach($keyword->relatedWord as $key => $related_word) --}}
					<div class="form-group">
						<div class="row">
							<div class="col-md-7">
								<input type="text" name="{{-- word_{{ ( isset($loop) ? $loop->parent->iteration : 0) }}_{{ $key }} --}}" class="form-control" value="{{-- {{ ( $related_word->text ?? '') }} --}}">
							</div>
							<div class="col-md-5">
								<select class="form-control" name="{{-- word_type_{{ ( isset($loop) ? $loop->parent->iteration : 0) }}_{{ $key }} --}}">
									<option>Szinonima</option>
									<option>Rövidítés</option>
								</select>
							</div>
						</div>
					</div>
					{{-- @endforeach --}}
				{{-- @endif --}}
			</div>
		</div>
	</p>
{{-- @endif --}}